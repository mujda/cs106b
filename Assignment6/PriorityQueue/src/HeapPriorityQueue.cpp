// This is a .cpp file you will edit and turn in.
// We have provided a skeleton for you,
// but you must finish it as described in the spec.
// Also remove these comments here and add your own.
// TODO: remove this comment header

#include "HeapPriorityQueue.h"

HeapPriorityQueue::HeapPriorityQueue() {
    root = NULL;

}

HeapPriorityQueue::~HeapPriorityQueue() {
    // TODO: implement

}

void HeapPriorityQueue::changePriority(string value, int newPriority) {
    // TODO: implement

}

void HeapPriorityQueue::clear() {
    // TODO: implement

}

string HeapPriorityQueue::dequeue() {
    // TODO: implement
    return "";   // remove this
}

void HeapPriorityQueue::enqueue(string value, int priority) {
    // TODO: implement

}

bool HeapPriorityQueue::isEmpty() const {
    // TODO: implement
    return false;   // remove this
}

string HeapPriorityQueue::peek() const {
    // TODO: implement
    return "";   // remove this
}

int HeapPriorityQueue::peekPriority() const {
    // TODO: implement
    return 0;   // remove this
}

int HeapPriorityQueue::size() const {
    // TODO: implement
    return 0;   // remove this
}

HeapPriorityQueue::node* HeapPriorityQueue::treeSearch(node *t, int priority) {
    if(t == NULL) return NULL;

    if(priority == t -> priority)
        return t;

    else if (priority < t -> priority)
        return treeSearch(t -> left, priority);
    else
        return treeSearch(t -> right, priority);
}

void HeapPriorityQueue::treeEnter(node *&t, string value, int priority) {
    if(t == NULL) {
        t = new node;
        t -> value = value;
        t -> priority = priority;
        t -> left = t -> right = NULL;
    } else if (priority == t -> priority) {
        t -> priority = priority;
    } else if (priority < t -> priority) {
        treeEnter(t -> left, value, priority);
    } else {
        treeEnter(t -> right, value, priority);
    }
}
ostream& operator<<(ostream& out, const HeapPriorityQueue& queue) {
    // TODO: implement
    return out;
}


