// This is the .cpp file that details the implementation
// for the Array-based Priority Queue class.

#include "ArrayPriorityQueue.h"
#include "vector.h"
#include "strlib.h"

using namespace std;

// Constructor: Initializes the priority Queue by
// creating the array and initializing the numUsed
// and numAllocated variables.
ArrayPriorityQueue::ArrayPriorityQueue() {

}

// Destructor: De-allocates the array used to
// store the priority queue elements
ArrayPriorityQueue::~ArrayPriorityQueue() {


}

// highPriorityIndex(): Is a helper function that
// goes through the array and returns the index of
// element that has the highest priority value
int ArrayPriorityQueue::highPriorityIndex() const {

    int high_priority = pqueue[0].priority;
    int index = 0;
    for(int i=1; i<pqueue.size(); i++) {
        if(pqueue[i].priority < high_priority)
            index = i;
    }

    return index;
}

// changePriority(string value, int newPriority): Replaces the priority
// for the element that corresponds to the given value with the new
// priority given
void ArrayPriorityQueue::changePriority(string value, int newPriority) {

    int index;
    for(int i=0; i<pqueue.size(); i++) {
        if(pqueue[i].value == value)
            index = i;
    }

    pqueue[index].priority = newPriority;

}

// clear(): Empties the array of all elements
void ArrayPriorityQueue::clear() {

    pqueue.clear();

}

// dequeue(): Returns the value with the highest
// priority value in the priority queue and removes
// the element from the priority queue
string ArrayPriorityQueue::dequeue() {

    int index = highPriorityIndex();
    string value = pqueue[index].value;
    pqueue.remove(index);

    return value;
}

// enqueue(string value, int priority): Adds the string value
// and corresponding priority number to the priority queue
void ArrayPriorityQueue::enqueue(string value, int priority) {
    PQEntry newEntry;
    newEntry.value = value;
    newEntry.priority = priority;

    pqueue.add(newEntry);
}

// isEmpty(): Returns a boolean of whether or not the
// priorityQueue is empty
bool ArrayPriorityQueue::isEmpty() const {
    return (pqueue.size() == 0);
}

// peek(): Returns the value with the highest priority
// in the priority queue without removing the element
// from the priority queue
string ArrayPriorityQueue::peek() const {
    int index = highPriorityIndex();
    return pqueue[index].value;
}

// peekPriority(): Returns the highest priority
// number in the priority queue without removing
// the element from the priority queue
int ArrayPriorityQueue::peekPriority() const {
    int index = highPriorityIndex();
    return pqueue[index].priority;
}

// size(): Returns the number of elements in the
// priority queue
int ArrayPriorityQueue::size() const {
    return pqueue.size();
}

// toString(ArrayPriorityQueue queue) returns a string
// representation of the priority queue
string ArrayPriorityQueue::toString(ArrayPriorityQueue queue) const{
    string queue_string = "";
    for(int i=0; i<queue.size(); i++) {
        PQEntry entry = pqueue[i];
        queue_string += entry.value + ":" + integerToString(entry.priority) + " , ";
    }

    return queue_string;
}

// Overloads the << operator to print the priority queue to the console
ostream& operator<<(ostream& out, const ArrayPriorityQueue& queue) {

    out << queue.toString(queue) << endl;
    return out;
}
