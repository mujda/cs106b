
#include "LinkedPriorityQueue.h"
#include "strlib.h"

LinkedPriorityQueue::LinkedPriorityQueue() {
    head = NULL;

}

LinkedPriorityQueue::~LinkedPriorityQueue() {
    for(ListNode *curr=head; curr !=NULL; curr = curr -> next)
        delete curr;

}

void LinkedPriorityQueue::changePriority(string value, int newPriority) {
    ListNode *changed;
    for(ListNode *curr=head; curr != NULL; curr = curr -> next) {
        if(curr -> value == value) {
            curr -> priority = newPriority;
            changed = curr;
        }
    }
    moveAfterChange(changed);
}

void LinkedPriorityQueue::moveAfterChange(ListNode *changed) {
    for(ListNode *curr=head; curr != NULL; curr = curr -> next) {
        if(curr -> next -> value == changed -> value)
            curr -> next = curr -> next -> next;
    }

    for(ListNode *curr=head; curr != NULL; curr = curr -> next) {
        if(curr -> next -> priority >= changed -> priority) {
            changed -> next = curr -> next;
            curr -> next = changed;
        }
    }
}

void LinkedPriorityQueue::clear() {
    for(ListNode *curr=head; curr != NULL; curr = curr -> next) {
        curr = NULL;
    }

}

string LinkedPriorityQueue::dequeue() {
    ListNode *old = head;
    string value = old -> value;
    head = head -> next;
    delete old;

    return value;
}


void LinkedPriorityQueue::enqueue(string value, int priority) {

    ListNode *newNode = new ListNode;
    newNode -> value = value;
    newNode -> priority = priority;

    if(isEmpty())
        head = newNode;

    else {
        ListNode *curr = head;

        while(curr -> next != NULL && curr -> next -> priority < priority) {
            curr = curr -> next;
        }

        if(curr -> next == NULL && curr -> priority > priority) {
            newNode -> next = curr;
            head = newNode;
        }

        else {
        newNode -> next = curr -> next;
        curr -> next = newNode;
        }

    }
}

bool LinkedPriorityQueue::isEmpty() const {
    return (head == NULL);
}

string LinkedPriorityQueue::peek() const {
    return head -> value;
}

int LinkedPriorityQueue::peekPriority() const {
    return head -> priority;
}

int LinkedPriorityQueue::size() const {
    int size = 0;
    for(ListNode *curr=head; curr != NULL; curr = curr -> next) {
        size ++;
    }
    return size;
}

string LinkedPriorityQueue:: toString() const{
    string queue_string = "";
    for(ListNode *curr=head; curr != NULL; curr = curr -> next) {
        queue_string += curr->value + ":" + integerToString(curr->priority) + ", ";
    }
    return queue_string;
}

ostream& operator<<(ostream& out, const LinkedPriorityQueue& queue) {
    out << queue.toString() << endl;
    return out;
}
