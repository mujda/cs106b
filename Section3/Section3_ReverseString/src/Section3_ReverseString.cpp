/* Section 3 : Problem 5 - ReverseString
 *
 * Write a function the retursn the string in reverse order.
 *
 * Consider both recursive and iterative techniques for solving
 * this problem.
 *
 */

#include "console.h"
#include "simpio.h"
#include <iostream>

using namespace std;

//Iterative Technique

string reverseString1(string str)
{
    string reverseStr;

    for(int i=0; i < str.length(); i++)
    {
        reverseStr += str.substr(str.length()-1 - i, 1);
    }

    return reverseStr;
}

string reverseString2(string str)
{
    int lastIndex = str.length()-1;

    if (str.length() == 1)
        return str;
    else
        return str[lastIndex] + reverseString2(str.substr(0,lastIndex));

}
 


int main() {

    string str = getLine("Enter string to reverse: ");
    cout<< "The reverse is: " << reverseString1(str) << " using iteration." << endl;
    cout<< "The reverse is: " << reverseString2(str) << " using recursion." << endl;
    return 0;
}
