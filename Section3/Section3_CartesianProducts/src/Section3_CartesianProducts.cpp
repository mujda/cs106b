/* Section 3: Problem 3 - Cartesian Products
 *
 * Wriet a function to compute the cartesian produce of two
 * sets of string sets.
 */

#include "console.h"
#include "simpio.h"
#include <iostream>

#include "set.h"
#include "strlib.h"

#include <iterator>

using namespace std;

struct pairT
{
    string first;
    string second;
};

Set<string> one;
Set<string> two;

void populateSets()
{
    one.add("A");
    one.add("B");
    one.add("C");

    two.add("X");
    two.add("Y");
}

int PairCmpFn(pairT one, pairT two)
{
    if (one.first == two.first && one.second == two.second)
        return 0;
    if (one.first < two.first)
        return -1;
    if (one.first == two.first && one.second < two.second)
        return -1;
    else
        return 1;
}

Set<pairT> CartesianProduct(Set<string> &one, Set<string> &two)
{
    Set<pairT> result (PairCmpFn);
    Set<string>::iterator oneIt = one.begin();

    while(oneIt != one.end())
    {
        string first = (*oneIt);
        Set<string>::iterator twoIt = two.begin();

        while(twoIt != two.end())
        {
            pairT pair;
            string second = (*twoIt);

            pair.first = first;
            pair.second = second;

            result.add(pair);
        }
    }

    return result;
}

int main() {

    populateSets();

    Set<pairT> product = CartesianProduct(one, two);

    Set<pairT>::iterator iter = product.begin();

    while(iter != product.end())
    {
        cout<< (*iter).first << (*iter).second << endl;
        iter++;
    }

    return 0;
}
