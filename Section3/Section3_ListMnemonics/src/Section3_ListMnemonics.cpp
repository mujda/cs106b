/* Section 3: Problem 8 - List Mnemonics
 *
 * Write a function that will generate all possible
 * letter combinations that correspond to a given number
 * on a standard Touch-Tone telephone dial.
 *
 */
 
#include "console.h"
#include "simpio.h"
#include <iostream>

#include "map.h"

using namespace std;

Map<string,string> letterMap;

void populateLetterMap(){
    letterMap.add("2", "ABC");
    letterMap.add("3", "DEF");
    letterMap.add("4", "GHI");
    letterMap.add("5", "JKL");
    letterMap.add("6", "MNO");
    letterMap.add("7", "PRS");
    letterMap.add("8", "TUV");
    letterMap.add("9", "WXY");
}

void RecursiveMnemonics(string prefix, string rest)
{
    if (rest.length() == 0)
        cout << prefix << endl;
    else
    {
        string options = letterMap.get(rest.substr(0,1));
        for (int i = 0; i < options.length(); i++)
        {
            RecursiveMnemonics(prefix+options[i], rest.substr(1));
        }
    }
}
void ListMnemonics(string str)
{
   RecursiveMnemonics("", str);
}


int main() {

    ListMnemonics("723");

    return 0;
}
