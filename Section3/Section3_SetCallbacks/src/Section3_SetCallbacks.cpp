/* Section 3: Problem 1: Set Callbacks
 *
 * Part A: Write a comparison call back for a Set
 * storing entryT (user-defined address book entry)
 * and show the necessary declaration for creating a
 * Set of entryT using this call back
 *
 * NOTE: With this setup, if the first and last name are the same
 * one of them will be considered a duplicate and will be deleted.
 *
 * Part B: Write a comparison function for a Set
 * containing strings that ignores case.
 */


#include "set.h"
#include "strlib.h"
#include <iterator>
#include "console.h"
#include "simpio.h"
#include <iostream>

using namespace std;

struct entryT {
    string firstName;
    string lastName;
    string phoneNumber;
};

entryT matin;
entryT mujda;
entryT hugh;

int compareFirstName(entryT a, entryT b)
{
    if (a.firstName < b.firstName)
        return -1;
    if (a.firstName > b.firstName)
        return 1;
    else
        return 0;
}
int compareEntry(entryT a, entryT b)
{
    if(a.lastName < b.lastName)
        return -1;
    if (a.lastName > b.lastName)
        return 1;
    else
        return compareFirstName(a,b);
}

Set <entryT> addressBook = Set<entryT> (compareEntry);

int compareWord(string a, string b)
{
    string newa = toLowerCase(a);
    string newb = toLowerCase(b);

    if (newa == newb)
        return 0;
    if (newa < newb)
        return 1;
    else
        return -1;

}

Set <string> stringSet = Set<string> (compareWord);

void populateAddressBook()
{
    matin.firstName = "Matin";
    matin.lastName = "Alamzai";
    matin.phoneNumber = "555-5555";

    mujda.firstName = "Mujda";
    mujda.lastName = "Alamzai";
    mujda.phoneNumber = "555-5556";

    hugh.firstName = "Hugh";
    hugh.lastName = "Oh";
    hugh.phoneNumber = "555-5557";

    addressBook.add(matin);
    addressBook.add(mujda);
    addressBook.add(hugh);
}

void populateStringSet()
{
    stringSet.add("Apple");
    stringSet.add("mango");
    stringSet.add("Mango");
}

int main()
{

    populateAddressBook();

    Set<entryT>::iterator it = addressBook.begin();

    while(it != addressBook.end())
    {
        cout<<(*it).firstName<< " " << (*it).lastName << " " <<(*it).phoneNumber << endl;
        it++;
    }

    populateStringSet();

    Set<string>:: iterator iter = stringSet.begin();

    while (iter != stringSet.end())
    {
        cout<<(*iter)<<endl;
        iter++;
    }


    return 0;
}


