/* Section 3: Problem 4 - Cannonballs
 *
 * Write a recursive function that takes as its argument the
 * height of pyramid and returns the number cannonballs therein.
 *
 * You know the stack is pyramid with one cannonball at the top,
 * sitting on top of a square composed of four cannonballs, sitting on
 * top of a square of nine cannonball, etc.
 */

#include "console.h"
#include "simpio.h"
#include <iostream>

using namespace std;

int balls;

int Cannonball (int height)
{
    if(height == 0)
        return 0;
    if(height < 1)
        return -1;
    else
        return height*height + Cannonball(height-1);
}

int main() {

    for(int i=0; i<4; i++)
    cout<< "If height is " << i << " there are " << Cannonball(i) << " cannonball(s)." << endl;

    return 0;
}
