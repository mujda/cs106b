/* Section 3 : Problem 7 - Old-Fashioned Measuring
 *
 * Write a recursive function the determines whether is its possible
 * to measure out the desired target amount with a given set of
 * weights.
 *
 * Fundamental observation:
 * Each weight in the vector can be either:
 * 1) Put on the opposite side of the balance from the sample
 * 2) Put on the same side of the balance as the sample
 * 3) Left off the balance entirely.
 *
 */
 
#include "console.h"
#include "simpio.h"
#include <iostream>

#include "vector.h"

using namespace std;

bool RecIsMeasurable(int target, Vector<int> &weights, int index)
{
    if (target == 0)
        return true;
    if (index >= weights.size())
        return false;

    else
        return (RecIsMeasurable(target + weights[index], weights, index+1) || RecIsMeasurable(target, weights, index+1) || RecIsMeasurable(target - weights[index], weights, index+1));

}

bool IsMeasurable(int target, Vector<int> &weights)
{
    return RecIsMeasurable(target, weights, 0);
}


int main() {

    Vector<int> sampleWeights;
    sampleWeights.add(1);
    sampleWeights.add(3);

    bool target2 = IsMeasurable(2, sampleWeights);
    bool target5 = IsMeasurable(5, sampleWeights);

    cout << target2 << endl;
    cout << target5 << endl;

    return 0;
}
