/* Section 3 : Problem 6 - GCD
 *
 * Write a function (using recursion) the computes the
 * greatest common divsor of int x and int y.
 *
 */
 
#include "console.h"
#include "simpio.h"
#include <iostream>

using namespace std;

int gcd(int x, int y)
{
    if(x % y == 0)
        return y;
    else
        return gcd(y, x%y);
}

int main() {

    int x = getInteger("Enter integer value for x: ");
    int y = getInteger("Enter integer value for y: ");

    cout<< "Their greatest common divisor is: " << gcd(x,y) << endl;

    return 0;
}
