/* Seciton 3: Problem 2 - Maps
 *
 * Write a program to manipulate a geographic mpas of major
 * world cities. When a user clicks on a point, report what
 * city is there.
 *
 * To efficiently support this operation, use a Map to associate
 * a point T with the nameof the city at that point.
 */

#include "map.h"
#include "strlib.h"
#include <iterator>
#include "console.h"
#include "simpio.h"
#include <iostream>

using namespace std;

struct pointT {
    int x;
    int y;
};

pointT ny;
pointT sf;
pointT uc;


struct cityT {
    string name;
    pointT coordinates;
};

cityT newYork;
cityT sanFran;
cityT unionCity;


Vector<cityT> cities;

void populateCities()
{
    ny.x = 3;
    ny.y = 3;

    sf.x = 2;
    sf.y = 2;

    uc.x = 1;
    uc.y = 1;

    newYork.coordinates = ny;
    sanFran.coordinates = sf;
    unionCity.coordinates = uc;

    newYork.name = "New York";
    sanFran.name = "San Francisco";
    unionCity.name = "Union City";

    cities.add(newYork);
    cities.add(sanFran);
    cities.add(unionCity);
}



Map<string,string> nameMap;




int main()
{
    populateCities();

    for (int cityIndex = 0; cityIndex < cities.size(); cityIndex++)
    {
        cityT currCity = cities[cityIndex];
        string xCoord = integerToString(currCity.coordinates.x);
        string yCoord = integerToString(currCity.coordinates.y);
        string key = xCoord + "-" + yCoord;
        nameMap.add(key, currCity.name);
    }

    string selectedPoint = getLine("Enter point: ");

    cout<< "City is " << nameMap.get(selectedPoint) << endl;

    return 0;
}
