/* Section #4 : Problem #6 - Append
 *
 * Write a function that given two lists will append the second list onto the first.
 * For example, given the first list (1 4 6) and the second list is (3, 19, 3) the
 * function would destructively modify the first list to contain (1 4 6 3 19 2).
 *
 */
 
#include "console.h"
#include "simpio.h"
#include <iostream>

#include "vector.h"

using namespace std;

struct Cell {
    Cell *next;
    int value;
};

Cell * ConvertToList(Vector<int> listOfValues) {
    Cell *list = NULL;
    for(int i=0; i < listOfValues.size(); i++) {
        Cell *newOne = new Cell;
        newOne -> value = listOfValues[i];
        newOne -> next = list;
        list = newOne;
    }
   return list;
}

void Append(Cell *&list1, Cell *list2) {
    if(list1 == NULL) {
        list1 = list2;
    }
    else {
        Append(list1 -> next, list2);
    }
}

void PrintList(Cell *list) {
    if(list != NULL) {
        cout << list -> value << endl;
        PrintList(list ->next);
    }
}

void Deallocate(Cell *list) {
    if(list != NULL) {
        Deallocate(list -> next);
        delete list;
    }
}

int main() {

    Vector<int> list1vals = {1, 4, 6};
    Cell *list1 = ConvertToList(list1vals);

    Vector<int> list2vals = {3, 19, 2};
    Cell *list2 = ConvertToList(list2vals);

    Append(list1, list2);
    PrintList(list1);

    Deallocate(list1);
//    Deallocate(list2);

    return 0;
}
