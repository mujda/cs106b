/* Section #4 : Problem #1 - Filling a Region
 *
 * Write a program that simulates the operation of the paint-bucket tool.
 * Write a function that paints the pixel that is clicked black along
 * with any pixels connected to that starting point by an unbroken
 * chain of white pixels.
 *
 */
 
#include "grid.h"
#include "simpio.h"
#include "console.h"
#include <iostream>

using namespace std;

enum pixelStateT {White, Black};

struct pointT {
    int row;
    int col;
};


void FillRegion(pointT pt, Grid<pixelStateT> &screen)
{
    if(!screen.inBounds(pt.row, pt.col)) return;
    if(screen.get(pt.row, pt.col) == Black) return;

    screen.set(pt.row, pt.col, Black);

    pointT north = {pt.row-1, pt.col};
    FillRegion(north, screen);

    pointT south = {pt.row+1, pt.col};
    FillRegion(south, screen);

    pointT west = {pt.row, pt.col-1};
    FillRegion(west, screen);

    pointT east = {pt.row, pt.col+1};
    FillRegion(east, screen);

}

Grid<pixelStateT> createGrid() {
    Grid<pixelStateT> screen = Grid<pixelStateT>(5, 10);

    screen.fill(White);

    for(int i=3; i<8; i++) {
        screen.set(2,i,Black);
    }
    screen.set(3,3, Black);
    screen.set(3,7, Black);
    screen.set(4,3, Black);
    screen.set(4,7,Black);

    return screen;
}


int main() {

    Grid<pixelStateT> screen = createGrid();

    pointT pt1;
    pt1.row = 0;
    pt1.col = 0;

    pointT pt2;
    pt2.row = 4;
    pt2.col = 4;


    FillRegion(pt2, screen);

    cout<< screen << endl;


    return 0;
}

