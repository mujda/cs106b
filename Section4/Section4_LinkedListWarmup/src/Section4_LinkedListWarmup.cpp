/* Section #4 - Problem #4 - Linked List Warmup
 *
 * a) Write a function ConvertToList which takes in a Vector of ints and
 * converts it into a linked list (assume the Vector has at least one
 * element in it)
 *
 * b) Write a function which sums the values of a linked list
 *
 */

#include "vector.h"

#include "console.h"
#include "simpio.h"
#include <iostream>

using namespace std;

Vector<int> listOfValues = {1,2,3,4,5,6,7,8,9,10};

struct Cell {
    Cell *next;
    int value;
};

Cell * ConvertToList(Vector<int> listOfValues) {
    Cell *list = NULL;
    for(int i=0; i < listOfValues.size(); i++) {
        Cell *newOne = new Cell;
        newOne -> value = listOfValues[i];
        newOne -> next = list;
        list = newOne;
    }
   return list;
}

void Deallocate(Cell *list) {
    if(list != NULL) {
        Deallocate(list -> next);
        delete list;
    }
}

void PrintList(Cell *list) {
    if(list != NULL) {
        cout << list -> value << endl;
        PrintList(list ->next);
    }
}

int SumList(Cell *list) {
    if (list == NULL) return 0;
    return list->value + SumList(list -> next);
}

int main() {
    Cell *list = ConvertToList(listOfValues);
 //   PrintList(list);
    cout << SumList(list) << endl;
    Deallocate(list);
    return 0;
}
