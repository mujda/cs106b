/* CreateHistograms()
 * Usage: CreateHistograms(Vector<int> scores)
 * --------------------------------------------
 * This function prints out a histogram of the distribution
 * of scores between 0 and 99.
 */

#include <iostream>
#include "simpio.h"
#include <fstream>
#include "vector.h"
#include "console.h"

using namespace std;

Vector <int> scores;

Vector<int> sortScores(Vector<int> scores);

string createStarString(int scoresInRange);

void createHistograms(Vector<int> sortedScores);

int main()
{
    string filename = "scores.txt";

    ifstream in;
    in.open(filename);
        if (in.fail()) cout << "Couldn't read ' " << filename << " ' ";

    while(true)
    {
        int score;
        in >> score;
            if(in.fail()) break;
        scores.add(score);
    }

    in.close();

    createHistograms(sortScores(scores));

    return 0;

}

Vector<int> sortScores(Vector<int> scores)
{
   int range0 = 0;
   int range1 = 0;
   int range2 = 0;
   int range3 = 0;
   int range4 = 0;
   int range5 = 0;
   int range6 = 0;
   int range7 = 0;
   int range8 = 0;
   int range9 = 0;

    for (int i=0; i < scores.size(); i++)
    {
        int score = scores.get(i);

        if(score >= 0 && score <= 9)
            range0++;
        if (score >= 10 && score <= 19)
            range1++;
        if (score >= 20 && score <= 29)
            range2++;
        if (score >= 30 && score <= 39)
            range3++;
        if (score >= 40 && score <= 49)
            range4++;
        if (score >= 50 && score <= 59)
            range5++;
        if (score >= 60 && score <= 69)
            range6++;
        if (score >= 70 && score <= 79)
            range7++;
        if (score >= 80 && score <= 89)
            range8++;
        if (score >= 90 && score <= 99)
            range9++;
    }

    Vector <int> sortedScores;
    sortedScores.add(range0);
    sortedScores.add(range1);
    sortedScores.add(range2);
    sortedScores.add(range3);
    sortedScores.add(range4);
    sortedScores.add(range5);
    sortedScores.add(range6);
    sortedScores.add(range7);
    sortedScores.add(range8);
    sortedScores.add(range9);

    return sortedScores;
}

void createHistograms(Vector <int> sortedScores)
{
    Vector<string> stars;

    for (int i=0; i < sortedScores.size(); i++)
    {
        stars.add(createStarString(sortedScores.get(i)));
    }

    cout << "0 - 9: " << stars.get(0) << endl;
    cout << "10 - 19: " << stars.get(1) << endl;
    cout << "20 - 29: " << stars.get(2) << endl;
    cout << "30 - 39: " << stars.get(3) << endl;
    cout << "40 - 49: " << stars.get(4) << endl;
    cout << "50 - 59: " << stars.get(5) << endl;
    cout << "60 - 69: " << stars.get(6) << endl;
    cout << "70 - 79: " << stars.get(7) << endl;
    cout << "80 - 89: " << stars.get(8) << endl;
    cout << "90 - 99: " << stars.get(9) << endl;
}

string createStarString(int scoresInRange)
{
   string starString;

        for (int j=0; j < scoresInRange; j++)
        {
            starString += '*';
        }

   return starString;
}
