/*
 * Project: Assignment1_Soundex
 *
 * Gets a name from the user and returns the
 * Soundex code.
 */

#include <iostream>
#include "simpio.h"
#include <string>
#include "strlib.h"
#include "console.h"

using namespace std;

const int CODE_LENGTH = 4;

string ParseName(string name);
char ParseChar(char c);

int main()
{
    while(true)
    {
        cout << "Enter surname (RETURN to quit): ";
        string surname = getLine();
        if (surname == "") exit(0);
        cout << "Soundex code for " << surname << " is " << ParseName(surname) << endl;
    }
}

string ParseName (string name)
{
    string code;
    name = toUpperCase(name);

    for (int i=0; i < name.length(); i++)
    {
        if (i==0)
            code += name [i];
        if (i > 0 && isalpha(name[i]) != 0)
            code += ParseChar(name[i]);
    }

    for (int j=0; j < code.length(); j++)
    {
        if (j>0 && code[j] == code[j-1])
            code.erase(j,1);
        if (code[j] == '0')
            code.erase(j,1);
    }

    if (code.length() < CODE_LENGTH)
    {
        int zeros = CODE_LENGTH - code.length();
        for (int k=0; k < zeros; k++)
            code += '0';
    }

    if (code.length() > CODE_LENGTH)
    {
        code = code.substr(0,CODE_LENGTH);
    }

    return code;
}

char ParseChar(char c)
{
    if (c == 'A' || c == 'E' || c == 'I' || c == 'O' ||c == 'U' || c == 'H' || c == 'W' || c == 'Y')
    {
        return '0';
    }

    if (c == 'B' || c == 'F' || c =='P' || c == 'V')
    {
        return '1';
    }

    if (c == 'C' || c == 'G' || c == 'J' || c == 'K' ||c == 'Q' || c == 'S' || c == 'X' || c == 'Z')
    {
        return '2';
    }

    if (c == 'D' || c == 'T')
    {
        return '3';
    }

    if (c == 'M' || c == 'N')
    {
        return '4';
    }

    if (c == 'L')
    {
        return '5';
    }

    else
    {
        return '6';
    }
}
