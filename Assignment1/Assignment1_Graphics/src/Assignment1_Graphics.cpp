/*
 * Project: Assign1graphics
 *
 * Asks the user to click three points on the window.
 * The program then draws a triangle from those points and
 * starts an animation. The user exits the program from a subsequent
 * mouse click.
 *
 */


#include "gwindow.h"
#include "gobjects.h"
#include "gevents.h"
#include "random.h"
#include "point.h"
#include "vector.h"

using namespace std;

const double CIRCLE_SIZE = .05; // size of small circle for animation

Point firstVertex;
Point secondVertex;
Point thirdVertex;

GWindow gw;

void createTriangle();
void setFirstCircle();

int main ()
{

    createTriangle();
    setFirstCircle();
    return 0;
}

void createTriangle()
{
    int count = 0;
    while (count < 3)
    {
        GMouseEvent e = waitForClick();

        double x = e.getX();
        double y = e.getY();

        if (count == 0)
        {
            firstVertex = Point(x,y);

        }

        if (count == 1)
        {
            secondVertex = Point(x,y);

            gw.drawLine(firstVertex.getX(), firstVertex.getY(), secondVertex.getX(), secondVertex.getY());

        }

        else {
            thirdVertex = Point(x,y);

            gw.drawLine(thirdVertex.getX(), thirdVertex.getY(), secondVertex.getX(), secondVertex.getY());
            gw.drawLine(thirdVertex.getX(), thirdVertex.getY(), firstVertex.getX(), firstVertex.getY());
        }

        count++;
    }
}

Point randPoint()
{
    int rand = randomInteger(1,3);
    Point pt;
    if (rand == 1) pt = firstVertex;
    if (rand == 2) pt = secondVertex;
    else pt = thirdVertex;
    return pt;
}

void setFirstCircle()
{
    Point currentPoint = randPoint();
    GOval* circle = new GOval (currentPoint.getX(), currentPoint.getY(), CIRCLE_SIZE, CIRCLE_SIZE);
    circle->isFilled();
    gw.add(circle);

}


