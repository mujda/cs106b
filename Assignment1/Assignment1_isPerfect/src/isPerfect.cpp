#include <iostream>
#include "simpio.h"
#include "console.h"

using namespace std;

int sumOfFactors(int num)
{
    int sum = 1;
    for (int i=2; i <= num/2; i++)
    {
        if (num % i ==0)
            sum += i;
    }
  return sum;
}

bool isPerfect(int num)
{
    int sum = sumOfFactors(num);
    if (num == sum)
        return true;
    else
        return false;
}

int main()
{
    for (int i = 1; i <= 10000; i++){
        if (isPerfect(i))
            cout<<i<<" is perfect." << endl;
    }
    return 0;
}
