
#include <iostream>
#include "simpio.h"
#include "random.h"
#include "console.h"

using namespace std;


double calculateError (int numOfVoters, double percentSpread, double errorPercent)
{
    double numInvalidElections = 0;
    int numVoteDiff = numOfVoters * percentSpread;
    for (int j = 0; j < 500; j++)
    {
        int votesForA = 0;
        int votesForB = 0;
        for (int i = 0; i < numOfVoters; i++)
        {
            bool vote = randomChance(1.0-errorPercent);
            if (i < (numOfVoters/2) + numVoteDiff) {
                if (vote) votesForA++;
                else votesForB++;
            } else {
                if (vote) votesForB++;
                else votesForA++;
            }
        }
            if (votesForB > votesForA)
                numInvalidElections++;
    }

    return double ((numInvalidElections/500) * 100);
}

int main() {

    int numOfVoters = getInteger("Enter number of voters: ");
        while(numOfVoters < 0)
           numOfVoters = getInteger("INCORRECT ENTRY: Re-enter number of voters: ");
    double percentSpread = getDouble("Enter percentage spread between candidates: ");
        while(percentSpread < 0 || percentSpread > 1.0)
           percentSpread = getDouble("INCORRECT ENTRY: Re-enter percentage spread between candidates: ");
    double errorPercent = getDouble("Enter voting error percentage: ");
        while(errorPercent < 0 || errorPercent > 1.0)
            errorPercent = getDouble("INCORRECT ENTRY: Re-enter voting error percentage: ");

    double chance = calculateError(numOfVoters, percentSpread, errorPercent);
    cout<< "Chance of invalid election result after 500 trials = % " << chance << endl;

    return 0;
}
