
/* CalculateStatistics()
 * Usage: stats = CalculateStatistics(filename)
 * --------------------------------------------
 * This function keeps track of the running low/high value
 * as it reads the file, as well as a total and a count to compute
 * the average when we're done
 */

#include <iostream>
#include "simpio.h"
#include <fstream>

using namespace std;

struct statsT {
    int low;
    int high;
    double average;
};

statsT CalculateStatistics(string filename) {
    statsT stats;
    stats.low = 101;
    stats.high = -1;
    
    int total = 0;
    int count = 0;

    ifstream in;
    in.open(filename.c_str());
    if (in.fail()) cout << "Couldn't read ' " << filename << " ' ";
    
    while (true) {
        int num;
        in >> num;
        if (in.fail()) break;
        if (num < stats.low) stats.low = num;
        if (num > stats.high) stats.high = num;
        total += num;
        count++;
    }
    
    stats.average = double(total)/count;
    in.close();
    return stats;
}

int main ()
{
    statsT stats = CalculateStatistics("scores.txt");
    cout << "Low score: " << stats.low << endl;
    cout << "High score: " << stats.high << endl;
    cout << "Average score: " << stats.average << endl;
    return 0;
}
