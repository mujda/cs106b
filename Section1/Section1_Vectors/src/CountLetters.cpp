#include <fstream>
#include <iostream>
#include "simpio.h"
#include "vector.h"
#include "strlib.h"

using namespace std;

const int AlphabetSize = 26;

void CountLetters(string filename)
{
    // Open a new filestream and make sure it worked
    ifstream in;
    in.open(filename.c_str());
    if (in.fail()) cout <<"Couldn't read '" << filename << "'";
    
    Vector<int> result;
    
    for (int i = 0; i < AlphabetSize; i++)
    {
        result.add(0); // must initialize contents
    }
    
    string line;
    
    while(true)
    {
        getline(in, line);
        // Check that we got a line
        if (in.fail()) break;
        
        line = toLowerCase(line);
        for (int j = 0; j < line.length(); j++)
        {
            char a = 'a';
            int index = line[j] - int(a);
            if(index >= 0 && index < AlphabetSize) {
                int prevTotal = result[index];
                result[index] = prevTotal + 1;
            }
        }
    }
    
    for(int k = 0; k < AlphabetSize; k++)
    {
        char a = 'a';
        int currLetterInt = int(a) + k;
        char currLetter = char(currLetterInt);
        cout << currLetter << ": " << result[k] << endl;
    }
}

int main()
{
    CountLetters("names.txt");
    return 0;
}
 
