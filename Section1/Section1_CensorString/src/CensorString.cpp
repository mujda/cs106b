//Section 1 Assignment
// Question#1 - Part 1 and 2
// Remove occurences of char in string
// Strategy #1 - Return entirely new string
// Strategy #2 - Return the same string with the characters removed

#include <iostream>
using namespace std;

string CensorString1 (string text, string remove)
{
    string result = "";

    for (int i=0; i < text.length(); i++)
    {
        bool found = false;
        for (int k=0; k < remove.length(); k++)
        {
            if (text[i] == remove [k])
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            result += text[i];
        }
    }

    return result;
}

void CensorString2 (string &text, string remove)
{
    for (int i=0; i < remove.length(); i++)
    {
        int pos = 0;

        while ((pos = text.find(remove[i], pos)) != string::npos)
        {
            text.replace(pos, 1, "");
        }
    }
}

int main() {
    string myString = "chihuahuas cheese crackers";

    cout << CensorString1("chihuahuas cheese crackers", "c") << endl;

    CensorString2(myString, "c");
    cout << myString << endl;
    return 0;
}
