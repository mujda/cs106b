/* Assignment 3: Warm-up A : Print in binary
 *
 * Write a recursive function that the prints the
 * binary representation for a given integer.
 *
 */
 
#include "simpio.h"
#include "console.h"
#include <iostream>

#include "vector.h"

using namespace std;

void PrintInBinary(int num)
{
    if(num > 1)
        PrintInBinary(num/2);
    cout << (num % 2);
}

bool CanMakeSum(Vector<int> &nums, int index, int sumSoFar, int target)
{
    if(sumSoFar == target) return true;
    if (index == nums.size()) return false;

    return CanMakeSum(nums, index+1, sumSoFar, target) || CanMakeSum(nums, index+1, sumSoFar+nums[index], target);

}


int main() {


    int number = getInteger("Enter decimal integer to convert to binary: ");
    PrintInBinary(number);

    Vector<int> nums = {3,7,1,8,-3};
    int target = getInteger("Enter target sum: ");

    cout<< CanMakeSum(nums, 0 , 0, 2) << endl;

    return 0;
}
