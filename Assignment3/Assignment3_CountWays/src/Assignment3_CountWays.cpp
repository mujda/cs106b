/* Assignment 3: Problem 1: Count Ways
 *
 * Write a recursive function that takes a positive numStairs
 * valuea nd returns the number of different ways to climb a
 * staircase of that heigh taking strides of one or two stairs
 * at a time.
 */
 
#include "console.h"
#include "simpio.h"
#include <iostream>

using namespace std;

const int SMALL_STRIDE = 1;
const int LARGE_STRIDE = 2;

int CountWaysRec(int stairsLeft, int stride)
{
    if (stairsLeft == stride) {
        return 1;
    }
    else if (stairsLeft < stride) {
        return 0;
    }

    return CountWaysRec(stairsLeft - SMALL_STRIDE, stride) + CountWaysRec(stairsLeft - LARGE_STRIDE, stride);
}

int CountWays(int numStairs)
{
    return CountWaysRec(numStairs, SMALL_STRIDE) + CountWaysRec(numStairs, LARGE_STRIDE);
}

int main() {

    while (true) {

        int numStairs = getInteger("Enter number of stairs (Enter -1 to terminate): ");

        if (numStairs == -1) break;

        cout << "Number of ways to climb staircase of " << numStairs << " steps is " << CountWays(numStairs) << endl;
    }

    return 0;
}
