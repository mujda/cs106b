/* Assignment 3 : Problem 2: Draw Ruler
 *
 * Write the recursive function DrawRuler(double x, double y, double w, double h)
 * that takes int he coordinates for a rectangle in which to draw the ruler. The
 * function draws a line along the rectangles bottome edge and a sequence of vertical
 * tick marks. The middle most tick mark is centered and is as tall as the rectangle
 * height. Each smaller tick mark is half the height of the next larger one.
 * Once the tick marks become sufficently small, the recursion terminates.
 *
 */
 
#include "graphics/gwindow.h"
#include "graphics/gobjects.h"

GWindow gw;

const int SPACER = 50;

void DrawTickMark(double x, double y, double w, double h)
{
    gw.drawLine(x+w/2, y, x+w/2, y+h);

    if (w>0.5) {
        DrawTickMark(x,y,w/2, h/2);
        DrawTickMark(x+w/2, y, w/2, h/2);
    }

}


void DrawRuler(double x, double y, double w, double h)
{
    gw.drawRect(x,y,w,h);
    DrawTickMark(x,y,w,h);
}

int main() {

    double w = gw.getCanvasWidth() - SPACER;
    double h = gw.getCanvasHeight() / 3;

    double x = gw.getCanvasWidth()/2 - w/2;
    double y = gw.getCanvasHeight()/2 - h/2;

    DrawRuler(x,y,w,h);

    return 0;
}
