/* Assignment #3 : Problem 4 : List Completions
 *
 * Write the function ListCompletions(string digits, Lexicon &lex)
 * that prints all words from the lexicon that can be formed by
 * extending the given digit sequence.
 *
 */
 
#include "console.h"
#include "simpio.h"
#include <iostream>

#include "lexicon.h"
#include "vector.h"
#include "map.h"

using namespace std;

const string VALID_DIGITS = "23456789";

Vector<string> mnemonics;
Map<string, string> letterMap;

void ListCompletions(string digits, Lexicon &lex);
void examineDigits(string digits, Lexicon &lex, string soFar);
string digitToLetters(string digit);
void printValidWords(string prefix, Lexicon &lex);
string scrubDigits(string input);

void populateLetterMap()
{
    letterMap.add("2", "ABC");
    letterMap.add("3", "DEF");
    letterMap.add("4", "GHI");
    letterMap.add("5", "JKL");
    letterMap.add("6", "MNO");
    letterMap.add("7", "PQRS");
    letterMap.add("8", "TUV");
    letterMap.add("9", "WXYZ");
}

int main() {
    Lexicon lex ("lexicon.dat");
    populateLetterMap();
    string input = getLine("Enter sequence of digits: ");
    cout << "Finding valid words that being with the dialed prefix " << input << "!" << endl;
    ListCompletions(input, lex);

    return 0;
}

void ListCompletions(string digits, Lexicon &lex)
{
    string cleanDigits = scrubDigits(digits);
    examineDigits(cleanDigits, lex, "");
}

string scrubDigits(string input)
{
    string output;
    for(int i=0; i < input.length(); i++) {
        if(VALID_DIGITS.find(input[i]) != string::npos)
            output += input[i];
    }

    return output;
}

void examineDigits(string digits, Lexicon &lex, string soFar)
{
    if(digits.length() == 0){
        printValidWords(soFar, lex);
    }
    else {
        string letters = digitToLetters(digits.substr(0,1));
        for (int j=0; j < letters.length(); j++) {
            examineDigits(digits.substr(1), lex, soFar + letters[j]);
        }
    }
}

string digitToLetters(string digit) {
    return letterMap.get(digit);
}

void printValidWords(string prefix, Lexicon &lex)
{
    string alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if(lex.contains(prefix)) cout << prefix << "" <<endl;

    if(lex.containsPrefix(prefix)){
        for(int i=0; i < alpha.length(); i++) {
            printValidWords(prefix + alpha[i], lex);
        }
    }
}
