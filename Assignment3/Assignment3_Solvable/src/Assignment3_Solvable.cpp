/* Assignment #3 : Problem #5 : Solvable
 *
 * Write a function bool Solvable(int start, Vector <int> & squares)
 * that takes a starting position of the marker along with the vector
 * of squares. The function will return true if it is possible to solve
 * the puzzle from the starting configuration and false if it is impossible.
 *
 * Assume all the integers in the vector are positive except for the last
 * entry, the goal sqare, which is always zero. The values of the elements
 * in the vector must be same after calling your function as beforehand.
 *
 */
 
#include <iostream>
#include "console.h"
#include "simpio.h"
#include "vector.h"

using namespace std;

bool Solvable(int start, Vector<int> &squares);

bool PuzzleSolvable(int start, Vector<int> & squares, Vector<bool> square_unused );


int main()
{
    Vector<int> puzzle;
    string puzzle_digits = "3641342530";

    puzzle.add(3);
    puzzle.add(6);
    puzzle.add(4);
    puzzle.add(1);
    puzzle.add(3);
    puzzle.add(4);
    puzzle.add(2);
    puzzle.add(5);
    puzzle.add(3);
    puzzle.add(0);

    if(Solvable(1, puzzle)) {
        cout<< "Yes, puzzle " << puzzle_digits << " has a solution." << endl;
    }
    else {
        cout << "No solution found for puzzle " << puzzle_digits  << "." << endl;
    }

    return 0;
}

bool Solvable(int start, Vector<int> &squares)
{
    Vector<bool> square_unsused;

    for (int i=0; i < squares.size(); i++) {
        square_unsused.add(true);
    }

    bool ifSolvable = (PuzzleSolvable(start, squares, square_unsused));

    return ifSolvable;
}

bool PuzzleSolvable(int start, Vector<int> &squares, Vector<bool> square_unused)
{
    if(start == squares.size()-1) return true;

    int left_dest = start - squares[start-1];
    if (left_dest > 0 && square_unused[left_dest-1]) {
        square_unused[left_dest-1] = false;
        if (PuzzleSolvable(left_dest, squares, square_unused)) return true;
    }

    int right_dest = start + squares[start-1];
    if (right_dest < squares.size() && square_unused[right_dest-1]) {
        square_unused[right_dest-1] = false;
        if(PuzzleSolvable(right_dest, squares, square_unused)) return true;
    }

    return false;
}
