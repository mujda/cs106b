/* Assignment #3 : Problem #3 : Count Critical Votes
 *
 * Write recursive function CountCriticalVotes(Vector<int> &blocks, int blockIndex)
 * which is given a vector of block vote counts and an index within the vector.
 * The function counts the number of election outcomes in which the block at the given
 * index has a critical vote.
 *
 */
 
#include "console.h"
#include "simpio.h"
#include <iostream>

#include "vector.h"

using namespace std;

Vector<int> blocks = {4,2,7,4};

int RecCountCriticalVotes(Vector<int> &blocks, int votesToWin, int Index, int Alice, int Bob)
{
    cout << Alice << endl;
    cout << Bob << endl;

    if(Alice >= votesToWin || Bob >= votesToWin) {
       return 0;
    }
    else if (Alice < votesToWin && Bob < votesToWin) {
        return 1;
    }

    blocks.remove(Index);

    return RecCountCriticalVotes(blocks, votesToWin, Index, Alice+blocks[Index], Bob) +
    RecCountCriticalVotes(blocks, votesToWin, Index, Alice, Bob+blocks[Index]);

}
int CountCriticalVotes(Vector<int> &blocks, int blockIndex)
{
    int sum = 0;
    for (int i=0; i < blocks.size(); i++) {
        sum += blocks[i];
    }
    int votesToWin = (sum/2) + 1;

    blocks.remove(blockIndex);

    return RecCountCriticalVotes(blocks, votesToWin, 0, blocks[0], 0) +
            RecCountCriticalVotes(blocks, votesToWin, 0 ,0, blocks[0]);
}

int main() {

    int blockIndex = getInteger("Enter block index: ");
    cout << "The number of election outcomes where the selected block is a critical vote is: "
         << CountCriticalVotes(blocks, blockIndex)  << endl;
    return 0;
}
