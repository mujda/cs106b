/* Assignment 3 : Problem # 6 - Stock Cutting
 *
 * Write the recursive function CutStock(Vector<int> &requests,
 * int stockLength) that returns the minimum number of stock pipes
 * needed to service all the requests in the vector.
 *
 */
 
#include "vector.h"

using namespace std;

int CutStock(Vector <int> &requests, int stockLength);
void PlacePipe(Vector<int> &requests, Vector<Vector<int> > &assignments, Vector<Vector<int> > &solution, int stockLength);
bool pieceFits(int request, Vector<int> assignment, int stockLength);
int printSolution(Vector<Vector<int> > &solution);


int main() {

    Vector <int> requests = {4,3,4,1,7,8};
    CutStock(requests, 10);

    return 0;
}

int CutStock(Vector<int> &requests, int stockLength) {
    Vector<Vector<int> > solution;
    Vector<Vector<int> > assignments;
    PlacePipe(requests, assignments, solution, stockLength);
    int min = printSolution(solution);

    return min;
}

void PlacePipe(Vector<int> &requests, Vector<Vector<int> > &assignments, Vector<Vector<int> > &solution, int stockLength) {

    if(requests.size() == 0) {
        if(assignments.size() < solution.size() || solution.size()==0) {
            solution = assignments;
        }
    }

    else {
        int request = requests[0];
        requests.remove(0);

        for(int i=0; i < assignments.size(); i++) {
            if(pieceFits(request, assignments[i], stockLength)) {
                assignments[i].add(request);
                PlacePipe(requests, assignments,solution, stockLength);
                assignments[i].remove(assignments[i].size()-1);
            }
        }

        Vector<int> new_vec;
        new_vec.add(request);
        assignments.add(new_vec);
        PlacePipe(requests, assignments, solution, stockLength);
    }
}

bool pieceFits(int request, Vector<int> assignment, int stockLength) {
    int sum_so_Far = 0;
    for(int i=0; i < assignment.size(); i++) {
        sum_so_Far += assignment[i];
    }

    if (request + sum_so_Far <= stockLength) return true;
    else return false;
}

int printSolution(Vector<Vector<int> > &solution) {
    cout << "The optimal solution uses " << solution.size() << " stock pieces." << endl;
    return solution.size();
}
