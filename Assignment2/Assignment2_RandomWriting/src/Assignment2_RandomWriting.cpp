/* Assignment 2: Part A - Random writing and Markov models of language
 *
 *
 *
 *
 */
 
#include <iostream>
#include "simpio.h"
#include "console.h"

#include <fstream>
#include "filelib.h"

#include "map.h"
#include "vector.h"

#include "random.h"

#include <iterator>


using namespace std;

Map<string, Vector<char> > seeds;

Map<string, int> seedFrequency;

Vector<string> readLines;

const int OUTPUT = 2000;

int order;


void storeFileData (string line)
{


    for(int i=0; i < line.length()-(order); i++)
    {
        string seed = line.substr(i,order);
        char nextChar = line[i+order];

        if(seeds.containsKey(seed))
           {
               Vector<char> possibleChars;
               possibleChars = seeds.get(seed);
               possibleChars.add(nextChar);
               seeds.add(seed, possibleChars);

               seedFrequency.add(seed, seedFrequency[seed]+1);
           }
        else
           {
               Vector <char> possibleChars;
               possibleChars.add(nextChar);
               seeds.add(seed,possibleChars);

               seedFrequency.add(seed, 1);
           }
    }
}

void readFile(string filename)
{
    ifstream file(filename);
    string line;

    if(file.is_open())
    {
        while(file.good())
        {
            getline(file, line);
            readLines.add(line);
        }
        file.close();
    }

    else
    {
        cout<< "Unable to open file" << endl;
        filename = getLine("Enter valid filename: ");
        readFile(filename);
    }
}

char getNextChar(string seed)
{
    Vector<char> nextChars = seeds.get(seed);

    if (nextChars.size() == 0)
    {
        return '%';
    }
    else
    {
       int high = nextChars.size() - 1;
       char nextChar = nextChars.get(randomInteger(0,high));
       return nextChar;
    }

}

string getInitialSeed(Map<string, int> seedFrequency)
{
    int max = 0;
    string initialSeed;
    Map<string, int> ::iterator it = seedFrequency.begin();

    while(it != seedFrequency.end())
    {
        int frequency = seedFrequency.get(*it);
        if(frequency >= max)
        {
            max = frequency;
            initialSeed = (*it);
        }
        it++;
    }

    return initialSeed;
}

string randomWriting(string currentSeed, int output)
{
    string randomText = currentSeed;

    char ch = getNextChar(currentSeed);

    if (ch == '%')
    {
       return randomText;
    }

    else
    {
        if (output == 0)
        {
            return randomText;
        }

        else
        {
            randomText += ch;
            currentSeed = randomText.substr(randomText.length() - order);
            return randomText += randomWriting(currentSeed, output-1);
        }
    }

}


int main() {

    string filename = getLine("Enter filename: ");
    order = getInteger("Enter desired order or Markov model: ");
        if(order < 1 || order > 10)
            order = getInteger("Invalid Value: Please enter value between 1 and 10: ");

    readFile(filename);

    for (int i=0; i < readLines.size(); i++)
    {
        string line = readLines.get(i);
        storeFileData(line);
    }

    cout << "Your random text: " << endl;
    cout << randomWriting(getInitialSeed(seedFrequency), OUTPUT) << endl;

    return 0;
}
