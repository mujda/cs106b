/* Assigment 5: Sorting - Problem 1: Sort Detective
 * --------------------------------------
 *
 * Run experiment to determine which of the following
 * algorithms (bubble sort, insertion sort, merge sort,
 * quicksort, and selection sort) correspond to the
 * mystery sort functions.
 *
 */

#include "mysterysort.h"
#include "vector.h"
#include "random.h"
#include "shuffle.h"
#include <ctime>

#include <iostream>
#include <simpio.h>
#include "console.h"

using namespace std;

Vector<int> createTestVector(int size);

template<typename ElemType>
int OperatorCmp(ElemType a, ElemType b) {
    if(a < b) return -1;
    if(a > b) return 1;
    else return 0;
}

template<typename ElemType>
void Swap(ElemType &a, ElemType &b) {
    ElemType tmp = a;
    a=b;
    b=tmp;
}

int getNextGap(int gap) {
    gap = (gap*10) / 13;

    if(gap < 1)
        return 1;
    return gap;
}

template <typename ElemType>
void CombSort(Vector<ElemType> &v, int (cmpFn) (ElemType, ElemType) = OperatorCmp){
    int gap = v.size();

    bool swapped = true;

    while(gap != 1 || swapped == true)
    {
        gap = getNextGap(gap);
        swapped = false;

        for (int i=0; i<v.size()-gap; i++) {
            if(v[i] > v[i+gap]) {
                Swap(v[i], v[i+gap]);
                swapped = true;
            }
        }
    }

}

int main() {
    Vector<int> test1 = createTestVector(10);
    cout<< "Unsorted: " << test1.toString() << endl;

    CombSort(test1);
    cout<< "Sorted: " << test1.toString() << endl;


    return 0;
}


Vector<int> createTestVector(int size) {
    Vector<int> output;
    for (int i=0; i<size; i++) {
        output.add(randomInteger(0,size));
    }

    return output;
}

