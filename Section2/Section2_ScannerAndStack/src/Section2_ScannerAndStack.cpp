/* Section 2: Problem 2: Using the Scanner and Stack Classes
 *
 * Write function to determine whether html tags are correctly
 * nested in a given html string.
 */

#include "stack.h"
#include "tokenscanner.h"
#include "string.h"
#include <iostream>
#include "simpio.h"
#include "console.h"

using namespace std;

bool ProcessOpenTag(TokenScanner& scanner, Stack<string>& tagStack)
{
    string tag = scanner.nextToken();
    tagStack.push(tag);

    return true;
}

bool ProcessCloseTag(TokenScanner& scanner, Stack<string>& tagStack)
{
    string tag = scanner.nextToken();

    if (!tagStack.isEmpty() && tag == tagStack.pop()) {
        return true;
    } else {
        return false;
    }
}

bool ProcessTag(TokenScanner& scanner, Stack<string>& tagStack)
{
    string token = scanner.nextToken();
    if (token == "/")
    {
        return ProcessCloseTag(scanner, tagStack);
    }
    else
    {
        scanner.saveToken(token);
        return ProcessOpenTag(scanner, tagStack);
    }
}

bool isCorrectlyNested(string htmlStr)
{
    TokenScanner scanner;
    scanner.ignoreWhitespace();

    Stack<string> tagStack;
    scanner.setInput(htmlStr);

    bool isBalanced = true;

    while (scanner.hasMoreTokens())
    {
        string token = scanner.nextToken();

        if (token == "<")
        {
            if (!ProcessTag(scanner, tagStack))
            {
                isBalanced = false;
                break;
            }
            scanner.nextToken();
        }
    }

    if (!tagStack.isEmpty()) isBalanced = false;
    return isBalanced;
}

int main()
{
    string htmlStr = getLine("Enter html string to check for nesting: ");
    bool correct = isCorrectlyNested(htmlStr);

    if (correct)
        cout<< "String is correctly nested." << endl;
    else
        cout<< "String is incorrectly nested. Please revise." << endl;
    return 0;
}
