/* Section 2: Problem 2: Queues
 *
 * Write a function ReverseQueue(Queue<int> &q) that
 * reverses the elements in the passed queue.
 *
 * NOTE: A Stack would be much more effective for this.
 *
 */

#include "queue.h"
#include "console.h"
#include "vector.h"
#include "stack.h"

using namespace std;

Queue<int> q;


void ReverseQueue(Queue<int> q)
{
   Stack <int> stack;
   while (!q.isEmpty())
   {
       stack.push(q.dequeue());
   }

   while (!stack.isEmpty())
   {
       q.enqueue(stack.pop());
   }
}


int main ()
{
    Queue<int> queue = fillQueue(q);
    ReverseQueue(queue);

    for(int i=0; i < queue.size(); i++)
        cout<< queue.dequeue() << endl;
\

    return 0;
}
