/* Section 2: Problem 5: Minesweeper
 *
 * Write function that can be passed a Minesweeper (boolean)
 * grid by reference, and returns an int grid storing the count
 * of bombs in each neighborhood.
 *
 */

#include "grid.h"
#include "console.h"
#include <iostream>

Grid<bool> minesweeper = Grid<bool>(6,6);

bool LocationOnGrid (int row, int col, Grid<int> &bombCounts)
{
    return row >= 0 && col >=0 && row < bombCounts.numRows() && col < bombCounts.numCols();

}

void MarkBomb(int row, int col, Grid<int> &bombCounts)
{
    for (int bombRow = -1; bombRow <= 1; bombRow++)
    {
        for (int bombCol = -1; bombCol <= 1; bombCol++)
        {
            if(LocationOnGrid(bombRow+row, bombCol+col, bombCounts))
                bombCounts(bombRow+row, bombCol+col)++;
        }
    }
}

Grid<int> MakeGridOfCounts(Grid<bool> &minesweeper)
{
    Grid<int> bombCounts (minesweeper.numRows(), minesweeper.numCols());

    for (int row=0; row < minesweeper.numRows(); row++)
    {
        for (int col=0; col < minesweeper.numCols(); col++)
        {
            bombCounts(row,col) = 0;
        }
    }

    for (int row=0; row < minesweeper.numRows(); row++)
    {
        for (int col=0; col < minesweeper.numCols(); col++)
        {
            if(minesweeper(row,col))
            {
                MarkBomb(row,col,bombCounts);
            }
        }
    }

    return bombCounts;
}
int main()
{
    return 0;
}



