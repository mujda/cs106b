/* Section 2: Problem 2: Map Warm-up
 *
 * Write a function MostFrequentCharacter() that given an
 * input file stream, returns the character that occurs
 * most frequently and stores the number of times it
 * occurs in the reference parameter numOccurrences.
 *
 */

#include "map.h"
#include "vector.h"
#include "tokenscanner.h"
#include <iostream>
#include <fstream>
#include "simpio.h"

using namespace std;
int numOccurrences;

char mostFrequentCharacter(ifstream &in, int &numOccurrences)
{
    Map<string, int> charFrequencies;
    numOccurrences = 0;

    int nextChar;

    while ((nextChar = in.get()) != EOF)
    {

        string foundChar = "";
        foundChar += char(nextChar);

        int frequency = 1;
        if (charFrequencies.containsKey(foundChar))
            frequency = charFrequencies[foundChar] + 1;
        charFrequencies[foundChar] = frequency;
    }

    Vector<string> charFrequenciesKeys = charFrequencies.keys();
    Vector<string>::iterator it = charFrequenciesKeys.iterator();
    string maxCharacter = "";

    while (it.hasNext())
    {
        string character = it.next();
        int frequency = charFrequencies[character];

        if (frequency > numOccurrences)
        {
            maxCharacter = character;
            numOccurrences = frequency;
        }

    }

    return maxCharacter[0];
}

int main ()
{
    ifstream in("file.txt");
        if(in.fail())
            cout<< "ERROR: File cannot be found.";
        cout<< "The most frequenent character is: " << mostFrequentCharacter(in, numOccurrences) << endl;
    return 0;
}

