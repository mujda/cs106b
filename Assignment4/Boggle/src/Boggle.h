// This is a .h file you will edit and turn in.
// We have provided a skeleton for you,
// but you must finish it as described in the spec.
// Also remove these comments here and add your own.
// TODO: remove this comment header

#ifndef _boggle_h
#define _boggle_h

#include <iostream>
#include "simpio.h"
#include <string>
#include "lexicon.h"

#include "grid.h"
#include "vector.h"
#include "set.h"
#include "random.h"

using namespace std;

class Boggle {

public:

    Boggle(Lexicon& dictionary, string boardText = "");
    char getLetter(int row, int col);
    bool checkWord(string word);
    bool humanWordSearch(string word);
    Set<string> computerWordSearch();
    int getScoreHuman();
    int getScoreComputer();


    friend ostream& operator<<(ostream& out, Boggle& boggle);

private:

    Grid<char> board;
    Lexicon ENGLISH_WORDS;

    Vector<string> CUBES;

    Set<string> humanWords;
    Set<string> computerWords;

};

#endif // _boggle_h
