// This is a .cpp file you will edit and turn in.
// We have provided a skeleton for you,
// but you must finish it as described in the spec.
// Also remove these comments here and add your own.
// TODO: remove this comment header

#include "Boggle.h"


Boggle::Boggle(Lexicon& dictionary, string boardText) {
    ENGLISH_WORDS = dictionary;
    string boardChars;

    if (boardText == "") {
        shuffle(CUBES);
        for(int i=0; i<CUBES.size(); i++) {
            string cube = CUBES[i];
            char ch = cube[randomInteger(0,cube.length())];
            boardChars += ch;
        }
    }

    else {
        boardChars = boardText;
        if (boardText.size() < 16 || boardText.size() > 16) {
            boardChars = getLine("Invalid board text entry: Enter 16 character string");
        }
    }

    for(int i=0; i<boardChars.size(); i++) {
        if(i<4) {
            board.set(0,i,boardChars[i]);
        }
        if(i>=4 && i<8) {
            board.set(1,i-4,boardChars[i]);
        }
        if(i>=8 && i<12) {
            board.set(2, i-8, boardChars[i]);
        }
        if(i>=12) {
            board.set(3, i-12, boardChars[i]);
        }
   }


}

char Boggle::getLetter(int row, int col) {

    if (!board.inBounds(row, col))
        cout<< "ERROR: OUT OF BOUNDS" << endl;
    return board.get(row,col);
}

bool Boggle::checkWord(string word) {
    if(ENGLISH_WORDS.contains(word) && word.size() >= 4
       && !humanWords.contains(word) && !humanWords.contains(word))
        return true;
    else
        return false;
}

bool Boggle::humanWordSearch(string word) {

string wordSoFar;

    if(wordSoFar == word)
        return true;

    for(int i=0; i<word.length(); i++) {

    }
}

int Boggle::getScoreHuman() {
    return humanWords.size();
}

Set<string> Boggle::computerWordSearch() {
    // TODO: implement
    Set<string> result;   // remove this
    return result;        // remove this
}

int Boggle::getScoreComputer() {
    return computerWords.size();
}


ostream& operator<<(ostream& out, Boggle& boggle) {
    return out;
}

